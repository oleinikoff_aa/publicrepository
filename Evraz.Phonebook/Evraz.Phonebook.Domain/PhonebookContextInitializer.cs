﻿using System.Data.Entity;

namespace Evraz.Phonebook.Domain
{
    class PhonebookContextInitializer : DropCreateDatabaseAlways<PhonebookContext>
    {
        protected override void Seed(PhonebookContext db)
        {

            db.PhoneTypes.Add(new Models.PhoneType { Id = "work", Name = "Рабочий" });
            db.PhoneTypes.Add(new Models.PhoneType { Id = "mobile", Name = "Мобильный" });

            var contactId = System.Guid.NewGuid().ToString();
            db.Contacts.Add(new Models.Contact { Id = contactId, Firstname = "Первый" });

            db.Phones.Add(new Models.Phone { Id = System.Guid.NewGuid().ToString(), ContactId = contactId, PhoneTypeId = "work", Number = "+7 (988) 999-99-99" });
            db.Phones.Add(new Models.Phone { Id = System.Guid.NewGuid().ToString(), ContactId = contactId, PhoneTypeId = "mobile", Number = "+7 (988) 888-88-88" });

            db.Contacts.Add(new Models.Contact { Id = System.Guid.NewGuid().ToString(), Firstname = "Второй" });

            db.SaveChanges();
        }
    }
}
