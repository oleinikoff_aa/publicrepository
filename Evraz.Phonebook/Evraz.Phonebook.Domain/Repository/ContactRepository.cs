﻿using Evraz.Phonebook.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evraz.Phonebook.Domain.Repository
{
    public class ContactRepository
    {

        public List<PhoneType> PhoneTypeList()
        {
            using (var context = new PhonebookContext())
            {
                return context.PhoneTypes.ToList();
            }
        }

        public List<ContactGrid> ContactList(string contactName)
        {
            using (var context = new PhonebookContext())
            {
                var list = (from contact in context.Contacts
                            where string.IsNullOrEmpty(contactName) 
                                  ? true 
                                  :(contact.Lastname.Contains(contactName)
                                 || contact.Firstname.Contains(contactName)
                                 || contact.Middlename.Contains(contactName)
                                 || contact.OrganizationName.Contains(contactName))
                            select new ContactGrid {
                                ContactId = contact.Id,
                                Firstname = contact.Firstname,
                                Lastname = contact.Lastname,
                                Middlename = contact.Middlename,
                                Description = contact.Description
                            }).ToList();

                return list;
            }
        }

        public Contact ContactItem(string id)
        {
            using (var context = new PhonebookContext())
            {
                return context.Contacts.Where(x => x.Id == id).FirstOrDefault();
            }
        }

        public void ContactRemove(string id)
        {
            using (var context = new PhonebookContext())
            {
                var contact = context.Contacts.Where(x => x.Id == id).FirstOrDefault();
                context.Contacts.Remove(contact);
                context.SaveChanges();
            }
        }

        public List<PhoneGrid> PhoneList(Phone filter)
        {
            using (var context = new PhonebookContext())
            {
                var list = (from contact in context.Contacts.Where(x=>x.Id== filter.ContactId)
                            join phone in context.Phones on contact.Id equals phone.ContactId
                            join phoneType in context.PhoneTypes on phone.PhoneTypeId equals phoneType.Id
                            where string.IsNullOrEmpty(filter.Number)
                                  ? true
                                  : (phone.Number.Contains(filter.Number)
                                 )
                            select new PhoneGrid
                            {
                                PhoneId = phone.Id,
                                PhoneTypeName = phoneType.Name,
                                PhoneNumber = phone.Number
                            }).ToList();

                return list;
            }
        }

        public Phone PhoneItem(string id)
        {
            using (var context = new PhonebookContext())
            {
                return context.Phones.Where(x => x.Id == id).FirstOrDefault();
            }
        }

        public void PhoneRemove(string id)
        {
            using (var context = new PhonebookContext())
            {
                var phone = context.Phones.Where(x => x.Id == id).FirstOrDefault();
                context.Phones.Remove(phone);
                context.SaveChanges();
            }
        }

        public void PhoneSave(Phone phone)
        {
            using (var context = new PhonebookContext())
            {
                if (string.IsNullOrWhiteSpace(phone.Id))
                {
                    phone.Id = System.Guid.NewGuid().ToString();
                    context.Phones.Add(phone);
                }
                else
                {
                    var contextPhone = context.Phones.Where(x => x.Id == phone.Id).FirstOrDefault();
                    contextPhone.PhoneType = phone.PhoneType;
                    contextPhone.Number = phone.Number;
                }

                context.SaveChanges();
            }
        }

        public void ContactSave(Contact contact)
        {
            using (var context = new PhonebookContext())
            {
                if (string.IsNullOrWhiteSpace(contact.Id))
                {
                    contact.Id = System.Guid.NewGuid().ToString();
                    context.Contacts.Add(contact);
                }
                else
                {
                    var contextContact = context.Contacts.Where(x => x.Id == contact.Id).FirstOrDefault();
                    contextContact.Lastname = contact.Lastname;
                    contextContact.Firstname = contact.Firstname;
                    contextContact.Middlename = contact.Middlename;
                    contextContact.OrganizationName = contact.OrganizationName;
                    contextContact.Description = contact.Description;
                }

                context.SaveChanges();
            }
        }
    }
}
