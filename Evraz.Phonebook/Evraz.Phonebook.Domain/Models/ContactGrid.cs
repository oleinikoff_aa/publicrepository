﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evraz.Phonebook.Domain.Models
{
    public class ContactGrid
    {
        public string PhoneId { get; set; }

        public string ContactId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Middlename { get; set; }

        public string OrganizationName { get; set; }

        public string Description { get; set; }

        public string PhoneTypeName { get; set; }

        public string PhoneNumber { get; set; }

        public string Fio => $"{Lastname} {Firstname} {Middlename}";

        public string ContactName => !string.IsNullOrWhiteSpace(Fio) ? Fio : OrganizationName;

    }
}
