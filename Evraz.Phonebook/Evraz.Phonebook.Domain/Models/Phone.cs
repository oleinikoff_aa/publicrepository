﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evraz.Phonebook.Domain.Models
{
    public class Phone
    {
        public string Id { get; set; }

        public string PhoneTypeId { get; set; }

        public string ContactId { get; set; }

        public string Number { get; set; }

        [ForeignKey("PhoneTypeId")]
        public virtual  PhoneType PhoneType { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }



        
    }
}
