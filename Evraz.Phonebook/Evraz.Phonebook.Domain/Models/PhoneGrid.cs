﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evraz.Phonebook.Domain.Models
{
    public class PhoneGrid
    {
        public string PhoneId { get; set; }

        public string PhoneTypeName { get; set; }

        public string PhoneNumber { get; set; }

        public string ContactId { get; set; }
    }
}
