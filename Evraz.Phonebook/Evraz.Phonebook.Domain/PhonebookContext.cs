﻿using Evraz.Phonebook.Domain.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Evraz.Phonebook.Domain
{
    public class PhonebookContext : DbContext
    {
        static PhonebookContext()
        {
            Database.SetInitializer<PhonebookContext>(new PhonebookContextInitializer());
        }

        public PhonebookContext() : base("PhonebookContext")
        {
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<PhoneType> PhoneTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
