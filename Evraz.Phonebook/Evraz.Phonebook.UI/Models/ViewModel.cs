﻿using Evraz.Phonebook.Domain.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Evraz.Phonebook.UI.Models
{
    public class ViewModel
    {
        public Contact Contact { get; set; }

        public Phone Phone { get; set; }


        public List<SelectListItem> PhoneTypeList { get; set; }
        public string CallbackOk { get; set; }
    }
}