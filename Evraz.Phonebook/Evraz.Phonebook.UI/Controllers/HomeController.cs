﻿using Evraz.Phonebook.Domain;
using Evraz.Phonebook.Domain.Models;
using Evraz.Phonebook.Domain.Repository;
using Evraz.Phonebook.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Evraz.Phonebook.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ContactRepository _contactRepository = new ContactRepository();

        public ActionResult Index()
        {
                return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult EditContact(ViewModel model)
        {
            if (model.Contact != null)
                model.Contact = _contactRepository.ContactItem(model.Contact.Id);

            return View(model);
        }

        public ActionResult EditPhone(ViewModel model)
        {
            if (model.Phone != null)
                model.Phone = _contactRepository.PhoneItem(model.Phone.Id);

            model.PhoneTypeList = _contactRepository.PhoneTypeList().Select(x => new SelectListItem { Value = x.Id, Text = x.Name }).ToList();

            return View(model);
        }

        public JsonResult ContactGrid(GridModel gridModel, string contactName)
        {
            try
            {
                var contactList = _contactRepository.ContactList(contactName);

                return Json(new { gridModel._search, gridModel.sidx, gridModel.sord, gridModel.page, rows = contactList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Message = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult PhoneGrid(GridModel gridModel, PhoneGrid filter)
        {
            try
            {
                var phoneList = _contactRepository.PhoneList(new Phone { ContactId = filter.ContactId, Number =filter.PhoneNumber});

                return Json(new { gridModel._search, gridModel.sidx, gridModel.sord, gridModel.page, rows = phoneList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> ContactAdd(ViewModel model)
        {
            try
            {
                await Task.Run(() => _contactRepository.ContactSave(model.Contact));

                return Json(new { success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> ContactRemove(string id)
        {
            try
            {
                await Task.Run(() => _contactRepository.ContactRemove(id));

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> PhoneAdd(ViewModel model)
        {
            try
            {
                await Task.Run(() => _contactRepository.PhoneSave(model.Phone));
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> PhoneRemove(string id)
        {
            try
            {
                await Task.Run(() => _contactRepository.PhoneRemove(id));

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}